import { OntologyAdminClientPage } from './app.po';

describe('ontology-admin-client App', () => {
  let page: OntologyAdminClientPage;

  beforeEach(() => {
    page = new OntologyAdminClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
