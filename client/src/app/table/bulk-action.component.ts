import {Component, Inject} from "@angular/core";
import {MAT_SNACK_BAR_DATA, MatDialog} from "@angular/material";
import {DeleteTripleComponent} from "../triples/delete-triple.component";
@Component({
  selector: 'bulk-action-snackbar-component',
  templateUrl: 'bulk-action-snackbar.html',
  styles: [`.example-pizza-party { color: hotpink; }`],
})
export class BulkActionComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any, public dialog: MatDialog) {
  }

  deleteTriples() {
    console.log(this.data);
    let dialogRef = this.dialog.open(DeleteTripleComponent, {
      width: '300px',
      data: this.data
    });


    /*   */
  }
}
