import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {DataSource} from "@angular/cdk/collections";
import {SelectionModel} from '@angular/cdk/collections';
import {MatSort, MatPaginator, MatIconRegistry, MatSnackBar} from '@angular/material';
import {Observable, BehaviorSubject, fromEvent, merge} from "rxjs";
import {distinctUntilChanged, map} from "rxjs/operators";
import {TriplesService} from "../triples/triples.service";
import {DomSanitizer} from "@angular/platform-browser";
import {BulkActionComponent} from "./bulk-action.component";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['table.component.sass']
})
export class TableComponent implements OnInit {

  tripleDatabase: any;
  selectedTriplesCount: number;
  triplesDatasource: TriplesDatasource | null;
  displayedColumns = ['select', 'graph', 'subject', 'predicate', 'object', 'action'];
  selection = new SelectionModel<TripleData>(true, []);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef;

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer, private triplesService: TriplesService, private snackBar: MatSnackBar) {

    iconRegistry.addSvgIcon('action-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/threedots.svg'));
    iconRegistry.addSvgIcon('delete-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/delete.svg'));
    iconRegistry.addSvgIcon('move-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/move.svg'));
    iconRegistry.addSvgIcon('copy-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/copy.svg'));
  }

  ngOnInit() {
    this.tripleDatabase = new TripleDatabase(this.triplesService);
    this.triplesDatasource = new TriplesDatasource(this.tripleDatabase, this.sort, this.paginator);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.triplesDatasource) {
          return;
        }
        this.triplesDatasource.filter = this.filter.nativeElement.value;
      })
  }

  tripleSelected(selection, row) {
    selection.toggle(row);
    this.selectedTriplesCount = this.selection.selected.length;
    if (this.selectedTriplesCount > 0) {
      this.snackBar.openFromComponent(BulkActionComponent, {
        data: this.selection.selected
      })
    }
  }


  /** Whether all filtered rows are selected. */
  isAllFilteredRowsSelected() {
    return this.tripleDatabase.data.every(data => this.selection.isSelected(data));
  }

  /** Whether the selection it totally matches the filtered rows. */
  isMasterToggleChecked() {
    return this.selection.hasValue() &&
      this.isAllFilteredRowsSelected() &&
      this.selection.selected.length >= this.tripleDatabase.data.length;
  }

  masterToggle() {
    if (this.isMasterToggleChecked()) {
      this.selection.clear();
    } else {
      this.tripleDatabase.data.forEach(data => this.selection.select(data));
    }
  }
}

export class TriplesDatasource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  constructor(private _tripleDatabase: TripleDatabase, private _sort: MatSort, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<TripleData[]> {

    const displayDataChanges = [
      this._tripleDatabase.dataChange,
      this._sort.sortChange,
      this._paginator.page,
      this._filterChange
    ];

    return merge(...displayDataChanges)
      .pipe(
        map(() => {
          const filteredData = this.getFilteredData();
          const sortedData = this.getSortedData(filteredData);
          const data = sortedData.slice();

          // Grab the page's slice of data.
          const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
          return data.splice(startIndex, this._paginator.pageSize);
        })
      );
  }

  disconnect() {
  }

  /** Returns a sorted copy of the database data. */
  getSortedData(filteredData: TripleData[]): TripleData[] {
    const data = filteredData;
    if (!this._sort.active || this._sort.direction == '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number|string = '';
      let propertyB: number|string = '';

      switch (this._sort.active) {
        case 'graph':
          [propertyA, propertyB] = [a.graph, b.graph];
          break;
        case 'subject':
          [propertyA, propertyB] = [a.subject, b.subject];
          break;
        case 'predicate':
          [propertyA, propertyB] = [a.predicate, b.predicate];
          break;
        case 'object':
          [propertyA, propertyB] = [a.object, b.object];
          break;
      }

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }

  getFilteredData(): TripleData[] {
    const data = this._tripleDatabase.data.slice();

    return data.slice().filter((item: TripleData) => {
      let searchStr = (item.subject + item.predicate + item.graph + item.object).toLowerCase();
      return searchStr.indexOf(this.filter.toLowerCase()) != -1;
    });
  }

}


export interface TripleData {
  graph: string;
  subject: string;
  predicate: string;
  object: string;

}

/** An example database that the data source uses to retrieve data for the table. */
export class TripleDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<TripleData[]> = new BehaviorSubject<TripleData[]>([]);

  get data(): TripleData[] {
    return this.dataChange.value;
  }

  constructor(private triplesService: TriplesService) {
    this.triplesService.awaitData('').subscribe(
      (triples) => {
        this.dataChange.next(triples);
      }
    );

  }

}

