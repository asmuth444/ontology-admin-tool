import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {TriplesModule} from "./triples/triples.modules";
import {GraphModule} from "./graph/graph.module";
import {AppComponent} from './app.component';
import {TriplesComponent} from "./triples/triples.component";
import {UriExtractorPipe} from './widgets/uri-extractor.pipe';
import {NvD3Module} from 'angular2-nvd3';
import {DialogModule} from "./dialog/dialog.module";
import {DialogComponent} from "./dialog/dialog.component";
import { HttpClientModule } from "@angular/common/http";
import {ContenteditableModelDirective} from './widgets/content-editable.directive';
import {GraphComponent} from "./graph/graph.component";
import {CdkTableModule} from "@angular/cdk/table";
import {DeleteTripleComponent} from "./triples/delete-triple.component";
import {SearchComponent} from './search/search.component';
import {AppRoutingModule} from "./app-routing.modules";
import {HomepageComponent} from './homepage/homepage.component';
import {BulkActionComponent} from './table/bulk-action.component';
import { TableComponent } from './table/table.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { CustomMaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
@NgModule({
  declarations: [
    AppComponent,
    TriplesComponent,
    DialogComponent,
    GraphComponent,
    DeleteTripleComponent,
    UriExtractorPipe,
    ContenteditableModelDirective,
    SearchComponent,
    HomepageComponent,
    BulkActionComponent,
    TableComponent,
    FileUploadComponent
  ],
  imports: [
    BrowserModule,
    CdkTableModule,
    FormsModule,
    HttpModule,
    NvD3Module,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    TriplesModule,
    GraphModule,
    DialogModule,
    HttpClientModule,
    CustomMaterialModule,
    FlexLayoutModule
  ],
  entryComponents: [DialogComponent, GraphComponent, DeleteTripleComponent,BulkActionComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
