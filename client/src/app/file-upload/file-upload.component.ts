import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TriplesService } from 'app/triples/triples.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  public fileData: ArrayBuffer | string;
  public triplesLoader: boolean;
  @Output() showGraph: EventEmitter<any> = new EventEmitter();

  constructor(private triplesService: TriplesService) { }


  ngOnInit() {
  }

  fileUpload(event) {
    let reader = new FileReader();
    reader.readAsText(event.srcElement.files[0]);
    let self = this;
    reader.onload = function () {
      self.fileData = reader.result.toString();
      self.triplesService.getTriplesFromFileContent(self.fileData)
        .subscribe(
          () => {
            self.showGraph.emit(true);
          }
        )
    }
  }

}
