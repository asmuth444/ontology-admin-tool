import { TestBed } from '@angular/core/testing';

import { FileParseService } from './file-parse.service';

describe('FileParseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileParseService = TestBed.get(FileParseService);
    expect(service).toBeTruthy();
  });
});
