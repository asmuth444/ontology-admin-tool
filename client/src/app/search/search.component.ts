import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";
import {ENTITIES} from "../entities";
import {DomSanitizer} from "@angular/platform-browser";
import {MatIconRegistry} from "@angular/material";
import { TriplesService } from 'app/triples/triples.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['search.component.scss']
})
export class SearchComponent implements OnInit {

  entities = ENTITIES;
  triplesLoader: boolean;

  @Input() uri: string;
  @Input() selectedEntity: string;
  @Output() showGraph: EventEmitter<any> = new EventEmitter();

  constructor(public router: Router, private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer, private TriplesService: TriplesService) {
    iconRegistry.addSvgIcon(
      'search-icon',
      sanitizer.bypassSecurityTrustResourceUrl('assets/SearchIcon.svg'));
  }

  viewTriples(uri, selectedEntity) {
    let formattedUri = uri.replace('#', ':');

    switch (selectedEntity) {
      case 'graph':
        this.TriplesService.getTriplesFromDB(formattedUri).subscribe(
          () => {
            this.showGraph.emit(true);
          }
        );
        break;

      case 'subject':
        this.TriplesService.getTriplesFromDBBySubject(formattedUri).subscribe(
          () => {
            this.showGraph.emit(true);
          }
        );
        break;

      case 'predicate':
        this.TriplesService.getTriplesFromDBByPredicate(formattedUri).subscribe(
          () => {
            this.showGraph.emit(true);
          }
        );
        break;
      case 'object':
        this.TriplesService.getTriplesFromDBByObject(formattedUri).subscribe(
          () => {
            this.showGraph.emit(true);
          }
        );
        break;
    }
  }

  getTriples() {
  }

  ngOnInit() {
  }

}
