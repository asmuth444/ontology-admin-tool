
import {Component, Inject} from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {TriplesService} from "./triples.service";
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'delete-triple-dialog.html',
})
export class DeleteTripleComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteTripleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,public TriplesService : TriplesService) { }

  cancel(): void {
    this.dialogRef.close();
  }

  deleteTriples(){
    let graphTriples = {
      triples: {}
    };

    this.data.forEach((triple) => {
      if (!graphTriples.triples.hasOwnProperty(triple.graph)) {
        graphTriples.triples[triple.graph + ""] = [];
      }

      graphTriples.triples[triple.graph].push({
        subject: triple.subject,
        object: triple.object,
        predicate: triple.predicate
      });
    });

    this.TriplesService.deleteTriples(graphTriples).subscribe(
      (res) => {
        this.dialogRef.close();
        /*this.refreshTriples(this.TriplesService.getUri());*/
        /*this.snackBar.open('Triples have been deleted successfully!', '', {
          duration: 2000,
        });*/
      }
    )
  }

}
