
import { throwError as observableThrowError, Observable, Subject, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { apiUrl } from "../shared/url.config";

@Injectable()
export class TriplesService {

  private triples = new BehaviorSubject<any>(undefined);
  private fetching: boolean;


  private getTriplesData() {
    return this.triples.asObservable();
  }

  awaitData(uri): Observable<any[]> {
    console.log(this.triples.getValue());
    if (!(this.triples.getValue()) && !this.fetching) {
      return this.getTriplesFromDB(uri);
    }
    return this.getTriplesData();
  }

  getTriplesFromDB(graphUri): Observable<any> {
    this.fetching = true;
    return this.http.get(apiUrl.graphs(graphUri))
      .pipe(
        map(this.extractTriplesData),
        catchError(this.handleError)
      );
  }

  getTriplesFromDBBySubject(subjectUri): Observable<any> {
    this.fetching = true;
    return this.http.get(apiUrl.subjects(subjectUri))
      .pipe(
        map(this.extractTriplesData),
        catchError(this.handleError)
      );
  }

  getTriplesFromDBByPredicate(predicateUri): Observable<any> {
    this.fetching = true;
    return this.http.get(apiUrl.predicates(predicateUri))
      .pipe(
        map(this.extractTriplesData),
        catchError(this.handleError)
      );
  }

  getTriplesFromDBByObject(objectUri): Observable<any> {
    this.fetching = true;
    return this.http.get(apiUrl.objects(objectUri))
      .pipe(
        map(this.extractTriplesData),
        catchError(this.handleError)
      );
  }

  getTriplesFromFileContent(fileContent: string): Observable<any> {
    this.fetching = true;
    return this.http.post(apiUrl.fileParse, {"fileContent" :fileContent})
      .pipe(
        map(this.extractTriplesData),
        catchError(this.handleError)
      );
  }

  constructor(private http: Http) {
  }

  editTriples(graphTriples): Observable<any> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});

    return this.http.post(apiUrl.editTriples, {graphTriples}, options)
      .pipe(
        map(res => res.json()),
        catchError(err => err)
      );
  }

  deleteTriples(graphTriples): Observable<any[]> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({
      headers: headers
    });

    return this.http.post(apiUrl.deleteTriples, {graphTriples}, options)
      .pipe(
        map(res => res.json()),
        catchError(err => err)
      );
  }

  private extractTriplesData = (res: Response) => {
    let body = res.json();
    this.fetching = false;
    for (let triple of body.triples) {
      triple.subject = triple.subject.value;
      triple.object = triple.object.value;
      triple.predicate = triple.predicate.value;
      triple.graph = triple.g.value;
    }
    this.triples.next(body.triples);

    return this.triples;
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    return observableThrowError(errMsg);
  }

}
