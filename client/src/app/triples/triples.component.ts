import {Component, OnInit, Input} from '@angular/core';
import {TriplesService} from './triples.service';
import {ActivatedRoute} from '@angular/router';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-triples',
  templateUrl: './triples.component.html',
  styleUrls: ['./triples.component.sass']
})
export class TriplesComponent implements OnInit {
  uri: string;
  selectedEntity: string;
  triples: any;
  triplesLoader: boolean;
  selectTableView: boolean;
  _data: any;
  @Input()
  set data(data: any) {
    if(data["mode"] === "file") {}
  }

  constructor(private route: ActivatedRoute, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private TriplesService: TriplesService) {
    iconRegistry.addSvgIcon('graph-view', sanitizer.bypassSecurityTrustResourceUrl('assets/graph_view.svg'));
    iconRegistry.addSvgIcon('graph-view-selected', sanitizer.bypassSecurityTrustResourceUrl('assets/graph_view_selected.svg'));
    iconRegistry.addSvgIcon('table-view', sanitizer.bypassSecurityTrustResourceUrl('assets/list.svg'));
    iconRegistry.addSvgIcon('table-view-selected', sanitizer.bypassSecurityTrustResourceUrl('assets/list_selected.svg'));
    this.triplesLoader = true;
    this.selectTableView = true;
  }

  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   this.selectedEntity = params['entityType'];
    //   this.uri = params['uri'];
    //   this.getTriples();
    // });
  }

  getTriples() {
    let formattedUri = this.uri.replace('#', ':');

    switch (this.selectedEntity) {
      case 'graph':
        this.TriplesService.getTriplesFromDB(formattedUri).subscribe(
          () => {
            this.triplesLoader = false;
          }
        );
        break;

      case 'subject':
        this.TriplesService.getTriplesFromDBBySubject(formattedUri).subscribe(
          () => {
            this.triplesLoader = false;
          }
        );
        break;

      case 'predicate':
        this.TriplesService.getTriplesFromDBByPredicate(formattedUri).subscribe(
          () => {
            this.triplesLoader = false;
          }
        );
        break;
      case 'object':
        this.TriplesService.getTriplesFromDBByObject(formattedUri).subscribe(
          () => {
            this.triplesLoader = false;
          }
        );
        break;
    }

  }

  displayGraphView() {
    this.selectTableView = false;
  }

  displayTableView() {
    this.selectTableView = true;
  }
}
