import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriplesComponent } from './triples.component';

describe('TriplesComponent', () => {
  let component: TriplesComponent;
  let fixture: ComponentFixture<TriplesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
