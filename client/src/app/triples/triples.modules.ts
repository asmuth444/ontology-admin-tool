import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TriplesService } from './triples.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [TriplesService]
})
export class TriplesModule { }
