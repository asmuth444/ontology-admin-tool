export const apiPath = {
  graphs: 'graphs',
  moveTriples: 'graphs/moveTriples',
  editTriples: 'graphs/editTriples',
  deleteTriples: 'graphs/deleteTriples',
  subjects: 'subjects',
  predicates: 'predicates',
  objects: 'objs',
  fileParse: 'parseFile'
};
