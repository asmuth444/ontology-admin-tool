import { environment } from "../../environments/environment";
import { apiPath } from "./endpoint.config";

export const apiUrl = {
  graphs: (uriComponent: string = undefined) => {
    return uriComponent ? `${environment.backendUrl}${apiPath.graphs}\/` + encodeURIComponent(uriComponent) : `${environment.backendUrl}${apiPath.graphs}`;
  },
  moveTriples: `${environment.backendUrl}${apiPath.moveTriples}`,
  editTriples: `${environment.backendUrl}${apiPath.editTriples}`,
  deleteTriples: `${environment.backendUrl}${apiPath.deleteTriples}`,
  subjects: (uriComponent: string = undefined) => {
    return uriComponent ? `${environment.backendUrl}${apiPath.subjects}\/` + encodeURIComponent(uriComponent) : `${environment.backendUrl}${apiPath.subjects}`;
  },
  predicates: (uriComponent: string = undefined) => {
    return uriComponent ? `${environment.backendUrl}${apiPath.predicates}\/` + encodeURIComponent(uriComponent) : `${environment.backendUrl}${apiPath.predicates}`;
  },
  objects: (uriComponent: string = undefined) => {
    return uriComponent ? `${environment.backendUrl}${apiPath.objects}\/` + encodeURIComponent(uriComponent) : `${environment.backendUrl}${apiPath.objects}`;
  },
  fileParse: `${environment.backendUrl}${apiPath.fileParse}`
}
