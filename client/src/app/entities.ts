export class Entity {
  value: string;
  text: string
}

export const ENTITIES: Entity[] = [{
  value: 'graph',
  text: 'GRAPH'
}, {
  value: 'subject',
  text: 'SUBJECT'
}, {
  value: 'predicate',
  text: 'PREDICATE'
}, {
  value: 'object',
  text: 'OBJECT'
}];
