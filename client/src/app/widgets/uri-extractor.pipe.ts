import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uriExtractor'
})
export class UriExtractorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var indexOfNameSpace = value.lastIndexOf("/");
    return value.substring(indexOfNameSpace+1);
  }

}
