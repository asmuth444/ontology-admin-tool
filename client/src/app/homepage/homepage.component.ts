import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  graphImageUrl = "./assets/graph.svg";
  selectedEntity = "graph";
  showData: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showGraph($event: any) {
    this.showData = true;
  }
}
