import { NgModule } from '@angular/core';
import {
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatSortModule,
  MatPaginatorModule,
  MatTooltipModule,
  MatGridListModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatTableModule,
  MatToolbarModule,
  MatTabsModule
} from '@angular/material';

import {PlatformModule} from '@angular/cdk/platform';
import {ObserversModule} from '@angular/cdk/observers';

@NgModule({
  declarations: [],
  exports: [
    MatDialogModule,
    MatSnackBarModule,
    MatGridListModule,
    MatSelectModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSortModule,
    MatTooltipModule,
    MatToolbarModule,
    MatTabsModule,
    ObserversModule,
    PlatformModule
  ]
})
export class CustomMaterialModule { }
