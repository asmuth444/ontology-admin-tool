import { Component } from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {DialogComponent} from './dialog/dialog.component';
import {MoveService} from "./dialog/move.service";
import {TriplesService} from "./triples/triples.service";
import * as _ from "lodash";
import {DeleteTripleComponent} from "./triples/delete-triple.component";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title: string;
  triples: any;
  uriData: any;

  constructor(public dialog: MatDialog, private MoveService : MoveService, private TriplesService : TriplesService, public snackBar: MatSnackBar) {
  	this.title = 'Semtech Admin Dashboard';
  }
/*
  initiateMove() {
    let dialogRef = this.dialog.open(DialogComponent);
    this.MoveService.setDialogReference(dialogRef);
  }

  displayDeleteTripleConfModal(){
    let displayDeleteTripleConfModalRef = this.dialog.open(DeleteTripleComponent);
    this.TriplesService.setDeleteTripleDialogReference(displayDeleteTripleConfModalRef);
  }

  save(){
    let modifiedTriples = this.TriplesService.getCachedTriplesData();
    this.uriData = this.TriplesService.getUri();
    this.TriplesService.getTriples(this.uriData.uri).subscribe(
      (triples) => {
        for (let triple of triples) {
          triple.graph = this.uriData.uri
        }
        let triplesToBeDeleted = _.differenceWith(triples,modifiedTriples, _.isEqual);
        let triplesToBeInserted = _.differenceWith(modifiedTriples,triples, _.isEqual);

        let graphTriples = {
          triplesToBeDeleted : triplesToBeDeleted,
          triplesToBeAdded : triplesToBeInserted,
          graphUri : this.uriData.uri
        }

        this.TriplesService.editTriples(graphTriples).subscribe(
          (res) => {
            this.snackBar.open('Triples have been updated successfully!', '', {
              duration: 2000,
            });
          }
        )

      }
    )

  }*/
}
