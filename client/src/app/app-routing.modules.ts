import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TriplesComponent} from "./triples/triples.component";
import {SearchComponent} from "./search/search.component";
import {HomepageComponent} from "./homepage/homepage.component";
const routes: Routes = [
  { path: '',  component: HomepageComponent },
  // { path: 'triples/:entityType/:uri', component: TriplesComponent}
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
