
import {throwError as observableThrowError, Observable, Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { apiUrl } from '../shared/url.config';

@Injectable()
export class GraphsService {

  private selectedGraph = new Subject<string>();
  selectedGraph$ = this.selectedGraph.asObservable();

  constructor(private http: Http) {
  }

  //Set Selected Graph
  setSelectedGraph(graphUri: string){
    this.selectedGraph.next(graphUri);
  }

  // Get all graphs from the API
  getGraphs(): Observable<any> {
    return this.http.get(apiUrl.graphs())
      .pipe(
        map(res => this.extractData(res)),
        catchError(err => this.handleError(err))
      );
  }

  private extractData(res: Response) {
    let body = res.json();
    let formatedGraphData = [];

    for (let graph of body.graphs) {
      formatedGraphData.push(graph.graph.value);
    }

    return formatedGraphData || {};
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return observableThrowError(errMsg);
  }

}
