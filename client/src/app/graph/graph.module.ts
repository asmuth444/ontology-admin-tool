import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GraphsService} from "./graphs.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [GraphsService]
})
export class GraphModule { }
