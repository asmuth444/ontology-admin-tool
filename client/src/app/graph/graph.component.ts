import {Component, OnInit} from '@angular/core';
import {TriplesService} from "../triples/triples.service";
import * as _ from "lodash";
declare let d3: any;

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['graph.component.sass']
})
export class GraphComponent implements OnInit {
  svg: any;
  dataset: any;

  constructor(private TriplesService: TriplesService) {

  }

  reloadGraph() {
    var nodes = this.svg.selectAll("circle");
    var graphEdges = this.dataset.edges;

    nodes.filter(function (node, index) {
      d3.select("circle#" + node.name.replace('#', '').replace(/ /g, "_")).style("opacity", 1);
      d3.select(".nodelabel#" + node.name.replace('#', '').replace(/ /g, "_")).style("opacity", 1);


      graphEdges.forEach(function (edge) {
        d3.select("line#edge" + node.name.replace('#', '').replace(/ /g, "_") + edge.target.name.replace('#', '').replace(/ /g, "_")).style("opacity", 1);
        d3.select(".edgelabel#" + node.name.replace('#', '').replace(/ /g, "_") + edge.target.name.replace('#', '').replace(/ /g, "_")).style("opacity", 1);
      })
    });
  }

  ngOnInit() {

    this.dataset = {
      nodes: [],
      edges: []
    };

    let triples;

    var w = 1000;
    var h = 600;
    var linkDistance = 230;

    var colors = d3.scale.category10();

    this.TriplesService.awaitData('').subscribe(
      (triplesData) => {
        triples = triplesData;
      }
    );

    for (let tripleObj of triples) {

      if (_.findIndex(this.dataset.nodes, function (o) {
          return o['name'] == tripleObj.subject;
        }) == -1) {
        let node = {};
        node['name'] = tripleObj.subject
        node['group'] = tripleObj.graph
        this.dataset.nodes.push(node);
      }

      if (_.findIndex(this.dataset.nodes, function (o) {
          return o['name'] == tripleObj.object;
        }) == -1) {
        let node = {};
        node['name'] = tripleObj.object
        node['group'] = tripleObj.graph
        this.dataset.nodes.push(node);
      }

      var edge = {
        source: _.findIndex(this.dataset.nodes, function (o) {
          return o['name'] == tripleObj.subject;
        }),
        target: _.findIndex(this.dataset.nodes, function (o) {
          return o['name'] == tripleObj.object;
        }),
        value: tripleObj.predicate.substr(tripleObj.predicate.lastIndexOf('/') + 1, tripleObj.predicate.length)
      }

      this.dataset.edges.push(edge);
    }

    for (let node of this.dataset.nodes) {
      if (node.name.lastIndexOf('/')) {
        node.name = node.name.substr(node.name.lastIndexOf('/') + 1, node.name.length);
      }
    }

    var node2neighbors = {};
    for (var i = 0; i < this.dataset.nodes.length; i++) {
      var graphDataNodes = this.dataset.nodes;
      var name = this.dataset.nodes[i].name;
      node2neighbors[name] = this.dataset.edges.filter(function (d) {
        return graphDataNodes[d.source].name == name;
      }).map(function (d) {
        return graphDataNodes[d.source].name == name ? graphDataNodes[d.target].name : graphDataNodes[d.source].name;
      });
    }

    this.svg = d3.select("#graph").append("svg").attr({"width": w, "height": h});

    var force = d3.layout.force()
      .nodes(this.dataset.nodes)
      .links(this.dataset.edges)
      .size([w, h])
      .linkDistance([linkDistance])
      .charge([-500])
      .theta(0.1)
      .gravity(0.05)
      .start();


    var edges = this.svg.selectAll("line")
      .data(this.dataset.edges)
      .enter()
      .append("line")
      .attr("id", function (d, i) {
        return 'edge' + d.source.name.replace('#', '').replace(/ /g, "_") + d.target.name.replace('#', '').replace(/ /g, "_")
      })
      .attr('marker-end', 'url(#arrowhead)')
      .style("stroke", "#9c9c9c")
      .style("pointer-events", "none");

    var nodes = this.svg.selectAll("circle")
      .data(this.dataset.nodes)
      .enter()
      .append("circle")
      .attr({"r": 15})
      .attr("id", function (n) {
        return n.name.replace('#', '').replace(/ /g, "_");
      })
      .style("fill", function (d, i) {
        return colors(3);
      })
      .call(force.drag)

    var graphEdges = this.dataset.edges;

    nodes.filter(function (n) {
      return true;
    })
      .on("dblclick", function (n) {

        d3.select("circle#" + n.name.replace('#', '').replace(/ /g, "_"))
          .style('fill', '#31CFE8')
          .attr({'r': 25});

        // Extract node's name and the names of its neighbors
        var name = n.name
          , neighbors = node2neighbors[name], threeLevelDeepNodes = neighbors;


        for (var i = 0; i < neighbors.length; i++) {
          var secondLevelDeepNode = node2neighbors[neighbors[i]];
          threeLevelDeepNodes = threeLevelDeepNodes.concat(secondLevelDeepNode);
          /*console.log(d3.select("circle#" + neighbors[i].replace('#','')));
           d3.select("circle#" + neighbors[i].replace('#','')).style("opacity", newOpacity);/!*
           d3.selectAll("line." + neighbors[i]).style("opacity", newOpacity);*!/!*/
        }

        if (secondLevelDeepNode) {
          for (var j = 0; j < secondLevelDeepNode.length; j++) {
            threeLevelDeepNodes = threeLevelDeepNodes.concat(node2neighbors[secondLevelDeepNode[j]]);
          }
        }

        console.log(threeLevelDeepNodes);

        var uniqThreeLevelDeepNode = _.uniq(threeLevelDeepNodes);
        uniqThreeLevelDeepNode.push(n.name);

        nodes.filter(function (node, index) {
          if (uniqThreeLevelDeepNode.indexOf(node.name) == -1) {
            d3.select("circle#" + node.name.replace('#', '').replace(/ /g, "_")).style("opacity", 0);
            d3.select(".nodelabel#" + node.name.replace('#', '').replace(/ /g, "_")).style("opacity", 0);

            var edgesOfNode = _.filter(graphEdges, function (edge) {
              return edge['source'].name === node.name;
            });

            edgesOfNode.forEach(function (edge) {
              d3.select("line#edge" + node.name.replace('#', '').replace(/ /g, "_") + edge['target'].name.replace('#', '').replace(/ /g, "_")).style("opacity", 0);
              d3.select(".edgelabel#" + node.name.replace('#', '').replace(/ /g, "_") + edge['target'].name.replace('#', '').replace(/ /g, "_")).style("opacity", 0);
            })
          }
        });

      });


    var nodelabels = this.svg.selectAll(".nodelabel")
      .data(this.dataset.nodes)
      .enter()
      .append("text")
      .style("font-size", "13px")
      .attr({
        "x": function (d) {
          return d.x;
        },
        "y": function (d) {
          return d.y;
        },
        "class": "nodelabel",
        "stroke": "#4a4a4a",
        "stroke-width": "1px",
        "id": function (d) {
          return d.name.replace('#', '').replace(/ /g, "_");
        }
      })
      .text(function (d) {
        return d.name;
      });

    var edgepaths = this.svg.selectAll(".edgepath")
      .data(this.dataset.edges)
      .enter()
      .append('path')
      .attr({
        'd': function (d) {
          return 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y
        },
        'class': 'edgepath',
        'fill-opacity': 0,
        'stroke-opacity': 0,
        'fill': '#ececec',
        'stroke': 'red',
        'id': function (d, i) {
          return 'edgepath' + i
        }
      })
      .style("pointer-events", "none");

    var edgelabels = this.svg.selectAll(".edgelabel")
      .data(this.dataset.edges)
      .enter()
      .append('text')
      .style("pointer-events", "none")
      .attr({
        'class': 'edgelabel',
        'id': function (d, i) {
          return d.source.name.replace('#', '').replace(/ /g, "_") + d.target.name.replace('#', '').replace(/ /g, "_")
        },
        'dx': 80,
        'dy': 0,
        'font-size': 13,
        'fill': '#4a4a4a'
      });

    edgelabels.append('textPath')
      .attr('xlink:href', function (d, i) {
        return '#edgepath' + i
      })
      .style("pointer-events", "none")
      .text(function (d, i) {
        return d.value
      });


    this.svg.append('defs').append('marker')
      .attr({
        'id': 'arrowhead',
        'viewBox': '-0 -5 10 10',
        'refX': 25,
        'refY': 0,
        //'markerUnits':'strokeWidth',
        'orient': 'auto',
        'markerWidth': 10,
        'markerHeight': 10,
        'xoverflow': 'visible'
      })
      .append('svg:path')
      .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
      .attr('fill', '#9c9c9c')
      .attr('stroke', '#9c9c9c');


    force.on("tick", function () {

      edges.attr({
        "x1": function (d) {
          return d.source.x;
        },
        "y1": function (d) {
          return d.source.y;
        },
        "x2": function (d) {
          return d.target.x;
        },
        "y2": function (d) {
          return d.target.y;
        }
      });

      nodes.attr({
        "cx": function (d) {
          return d.x;
        },
        "cy": function (d) {
          return d.y;
        }
      });

      nodelabels.attr("x", function (d) {
        return d.x;
      })
        .attr("y", function (d) {
          return d.y;
        });

      edgepaths.attr('d', function (d) {
        var path = 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y;
        //console.log(d)
        return path
      });

      edgelabels.attr('transform', function (d, i) {
        if (d.target.x < d.source.x) {
          let bbox = this.getBBox();
          let rx = bbox.x + bbox.width / 2;
          let ry = bbox.y + bbox.height / 2;
          return 'rotate(180 ' + rx + ' ' + ry + ')';
        }
        else {
          return 'rotate(0)';
        }
      });
    });

  }
}


