import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers} from "@angular/http";
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { apiUrl } from '../shared/url.config';

@Injectable()
export class MoveService {
  private dialogRef;

  constructor(private http: Http) { }

   moveTriples(graphTriples): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

      return this.http.post(apiUrl.moveTriples, {graphTriples}, options)
              .pipe(
                map(res => res),
                catchError(err => err)
              );
  }

  setDialogReference(dialog : any) {
    this.dialogRef = dialog;
  }

  getDialogReference(){
    return this.dialogRef;
}
}
