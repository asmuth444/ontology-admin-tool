import { Component, OnInit } from '@angular/core';
import { TriplesService } from './../triples/triples.service';
import { MoveService } from './move.service';
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
	destGraph: string;
	private triples: any;

  constructor(private TriplesService: TriplesService,
              private MoveService: MoveService,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
  }/*

  refreshTriples(uriData) {
    switch (uriData.entityType) {
      case 'Graph':
        this.TriplesService.getTriples(uriData.uri).subscribe(
          (triples) => {
            for (let triple of triples) {
              triple.graph = uriData.uri
            }
            this.triples = this.TriplesService.publishData(triples);
          }
        );
        break;

      case 'Subject':
        this.SubjectService.getById(uriData.uri.replace('#', ':')).subscribe(
          triples => this.triples = this.TriplesService.publishData(triples)
        )
        break;
      case 'Predicate':
        this.PredicateService.getById(uriData.uri.replace('#', ':')).subscribe(
          triples => this.triples = this.TriplesService.publishData(triples)
        )
        break;
      case 'Object':
        this.ObjService.getById(uriData.uri.replace('#', ':')).subscribe(
          triples => this.triples = this.TriplesService.publishData(triples)
        )
        break;
    }
  }


  moveSelectedGraph() {
  	let selectedTriples = this.TriplesService.getSelectedTriples();

  	if (selectedTriples.length > 0 && this.destGraph) {
      let graphTriples: GraphTriples = {
        triples : {},
        destGraph: this.destGraph
      };

  		selectedTriples.forEach((triple) => {
  			if(!graphTriples.triples.hasOwnProperty(triple.graph)) {
  				graphTriples.triples[triple.graph+""] = [];
  			}

  			graphTriples.triples[triple.graph].push({
  				subject : triple.subject,
      		object : triple.object,
      		predicate : triple.predicate
  			});
  		});

      this.MoveService.moveTriples(graphTriples)
        .subscribe(
          (triples) => {
            this.refreshTriples(this.TriplesService.getUri());
            let dialogRef = this.MoveService.getDialogReference();
            this.snackBar.open('Triples have been moved successfully!', '', {
            duration: 2000,
          });
            dialogRef.close();
          }
        )
  	}
  }*/
}

