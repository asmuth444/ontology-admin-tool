import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MoveService} from "./move.service";


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [MoveService]
})
export class DialogModule { }
