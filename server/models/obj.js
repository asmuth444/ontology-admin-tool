'use strict';
var Client = require('./../util/sparql-connection');
Client.setQueryMaxrows(100);

module.exports = function(Obj) {
    Obj.getAll = function (obj) {
        Client.query('select DISTINCT ?obj {?obj ?s ?p}')
            .then(function (results) {
                obj(null, results.results.bindings);
            })
            .catch(function (err) {
                console.log(err);
            });
    }

  function getWithSeparator(toValidate) {
    if (toValidate.indexOf("spo:") === -1) {
      return `"${toValidate}"`
    } else {
      return `${toValidate}`
    }
  }

    Obj.findById = function (objId, cb) {
      Client.query('SELECT *{ graph ?g { ?subject ?predicate ?object} FILTER (?object IN (' + getWithSeparator(objId) + '))}')
            .then(function (results) {
                cb(null, results.results.bindings);
            })
            .catch(function (err) {
                console.log(err);
            })
    }


    Obj.remoteMethod('findById', {
        http: {path: '/:id', verb: 'get'},
        accepts: {arg: 'id', type: 'string'},
        returns: {arg: 'triples', type: 'array'}
    });

    Obj.remoteMethod('getAll',{
        http: {path: '/', verb: 'get'},
        returns: {arg: 'objects', type: 'array'}
    });
};
