'use strict';
var Client = require('./../util/sparql-connection');
Client.setQueryMaxrows(100);

module.exports = function (Subject) {
  Subject.getAll = function (subject) {
    Client.query('select DISTINCT ?subject {?subject ?p ?o}')
      .then(function (results) {
        subject(null, results.results.bindings);
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  Subject.findById = function (subjectId, cb) {
    Client.query('SELECT *{ graph ?g { ?subject ?predicate ?object} FILTER (?subject IN (' + subjectId+ '))}')
      .then(function (results) {
        cb(null, results.results.bindings);
      })
      .catch(function (err) {
        console.log(err);
      })
  }

  Subject.remoteMethod('getAll', {
    http: {path: '/', verb: 'get'},
    returns: {arg: 'subjects', type: 'array'}
  });
  Subject.remoteMethod('findById',{
    http : {path: '/:id', verb: 'get'},
    accepts: {arg: 'id', type : 'string'},
    returns: {arg: 'triples' , type: 'array'}
  });
};
