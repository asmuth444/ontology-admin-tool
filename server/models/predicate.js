'use strict';
var Client = require('./../util/sparql-connection');
Client.setQueryMaxrows(100);

module.exports = function (Predicate) {

  Predicate.getAll = function (predicate) {
    Client.query('select DISTINCT ?predicate {?predicate ?s ?o}')
      .then(function (results) {
        predicate(null, results.results.bindings);
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  Predicate.findById = function (predicateId, cb) {
    Client.query('SELECT *{ graph ?g { ?subject ?predicate ?object } FILTER (?predicate IN (' + predicateId + '))}')
      .then(function (results) {
        cb(null, results.results.bindings);
      })
      .catch(function (err) {
        console.log(err);
      })
  }

  Predicate.remoteMethod('getAll', {
    http: {path: '/', verb: 'get'},
    returns: {arg: 'predicates', type: 'array'}
  });

  Predicate.remoteMethod('findById', {
    http: {path: '/:id', verb: 'get'},
    accepts: {arg: 'id', type: 'string'},
    returns: {arg: 'triples', type: 'array'}
  });
};
