'use strict';
var Client = require('./../util/sparql-connection');

module.exports = function (Graph) {
  Graph.getAll = function (graph) {
    Client.query('select DISTINCT ?graph {graph ?graph {?s ?p ?o}}')
      .then(function (results) {
        graph(null, results.results.bindings);
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  Graph.findById = function (graphUri, cb) {
    Client.query('select * {graph ?g {?subject ?predicate ?object } filter(?g in (' + graphUri + '))}')
      .then(function (results) {
        cb(null, results.results.bindings);
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  Graph.moveTriples = function (graphTriples, cb) {

    var res = {
      success: 0,
      failure: 0
    };

    for (var graphId in graphTriples.triples) {
      var structuredTriples = [],
        joinedTriples;

      graphTriples.triples[graphId].forEach(function (triple) {
        function getWithSeparator(toValidate) {
          if (toValidate.indexOf("http") > -1) {
            return '<${toValidate}>';
          } else {
            return '"${toValidate}"'
          }
        }

        structuredTriples.push('${getWithSeparator(triple.subject)} ${getWithSeparator(triple.predicate)} ${getWithSeparator(triple.object)}')
      })

      joinedTriples = structuredTriples.join('.\n')

      Client.query('WITH <${graphId}> DELETE { ${joinedTriples} }')
        .then(function (results) {
          updateStatus(1, cb);
        })
        .catch(function (err) {
          updateStatus(0, cb);
        });

      Client.query('WITH <${graphTriples.destGraph}> INSERT { ${joinedTriples} }')
        .then(function (results) {
          updateStatus(true, cb);
        })
        .catch(function (err) {
          updateStatus(false, cb);
        });
    }

    function updateStatus(status, cb) {
      if (status) {
        res.success++
      } else {
        res.failure++
      }

      if (res.success + res.failure === 2) {
        if (res.success === 2) {
          cb(null, {status: "success"});
        } else {
          cb(null, {status: "failure"});
        }
      }
    }
  }

  Graph.editTriples = function (graphTriples, cb) {

    var res = {
      success: 0,
      failure: 0
    };

    var structuredTriplesToBeDeleted = [], structuredTriplesToBeAdded = [],
      joinedTriplesToBeDeleted, joinedTriplesToBeAdded;

    graphTriples.triplesToBeDeleted.forEach(function (triple) {
      function getWithSeparator(toValidate) {
        if (toValidate.indexOf("http") > -1) {
          return '<${toValidate}>';
        } else {
          return '"${toValidate}"'
        }
      }

      structuredTriplesToBeDeleted.push('${getWithSeparator(triple.subject)} ${getWithSeparator(triple.predicate)} ${getWithSeparator(triple.object)}')
    });

    joinedTriplesToBeDeleted = structuredTriplesToBeDeleted.join('.\n')


    graphTriples.triplesToBeAdded.forEach(function (triple) {
      function getWithSeparator(toValidate) {
        if (toValidate.indexOf("http") > -1) {
          return '<${toValidate}>';
        } else {
          return '"${toValidate}"'
        }
      }

      structuredTriplesToBeAdded.push('${getWithSeparator(triple.subject)} ${getWithSeparator(triple.predicate)} ${getWithSeparator(triple.object)}')
    });

    joinedTriplesToBeAdded = structuredTriplesToBeAdded.join('.\n')

    Client.query('WITH <${graphTriples.graphUri}> DELETE { ${joinedTriplesToBeDeleted} }')
      .then(function (results) {
        updateStatus(1, cb);
      })
      .catch(function (err) {
        updateStatus(0, cb);
      });

    Client.query('WITH <${graphTriples.graphUri}> INSERT { ${joinedTriplesToBeAdded} }')
      .then(function (results) {
        updateStatus(true, cb);
      })
      .catch(function (err) {
        updateStatus(false, cb);
      });

    function updateStatus(status, cb) {
      if (status) {
        res.success++
      } else {
        res.failure++
      }

      if (res.success + res.failure === 2) {
        if (res.success === 2) {
          cb(null, {status: "success"});
        } else {
          cb(null, {status: "failure"});
        }
      }
    }

  }

  Graph.deleteTriples = function (graphTriples, cb) {
    //console.log(graphTriples)

    for (var graphId in graphTriples.triples) {
      var structuredTriplesToBeDeleted = [], joinedTriplesToBeDeleted, triplesToBeDeleted = [];

      triplesToBeDeleted = graphTriples.triples[graphId];

      triplesToBeDeleted.forEach(function (triple) {
        function getWithSeparator(toValidate) {
          if (toValidate.indexOf("http") > -1) {
            return `<${toValidate}>`;
          } else {
            return `"${toValidate}"`
          }
        }

        structuredTriplesToBeDeleted.push(`${getWithSeparator(triple.subject)} ${getWithSeparator(triple.predicate)} ${getWithSeparator(triple.object)}`)
      });

      joinedTriplesToBeDeleted = structuredTriplesToBeDeleted.join('.\n')
      Client.query(`WITH <${graphId}> DELETE { ${joinedTriplesToBeDeleted} }`)
        .then(function (results) {
          cb(null, {status: "success"});
        })
        .catch(function (err) {
          console.log(err);
          cb(null, {status: "failure"});
        });


    }
  }


  Graph.addTriples = function (triples, cb) {
    /*   var structuredTriplesToBeAdded = [], joinedTriplesToBeAdded;

     triples.forEach(function (triple) {
     function getWithSeparator(toValidate) {} else {
     if (toValidate.indexOf("http") > -1) {
     return '<${toValidate}>';
     return '"${toValidate}"'
     }
     }

     structuredTriplesToBeAdded.push('${getWithSeparator(triple.subject)} ${getWithSeparator(triple.predicate)} ${getWithSeparator(triple.object)}')
     });

     joinedTriplesToBeAdded = structuredTriplesToBeDeleted.join('.\n')

     Client.query('WITH <${graphId}> INSERT { ${joinedTriplesToBeAdded} }')
     .then(function (results) {
     cb(null, {status: "success"});
     })
     .catch(function (err) {
     cb(null, {status: "failure"});
     });*/

  }

  Graph.remoteMethod('getAll', {
    http: {path: '/', verb: 'get'},
    returns: {arg: 'graphs', type: 'array'}
  });

  Graph.remoteMethod('findById', {
    http: {path: '/:id', verb: 'get'},
    accepts: {arg: 'id', type: 'string'},
    returns: {arg: 'triples', type: 'array'}
  });

  Graph.remoteMethod('moveTriples', {
    http: {path: '/moveTriples', verb: 'post'},
    accepts: {arg: 'graphTriples', type: 'object'},
    returns: {arg: 'status', type: 'string'}
  });

  Graph.remoteMethod('editTriples', {
    http: {path: '/editTriples', verb: 'post'},
    accepts: {arg: 'graphTriples', type: 'object'},
    returns: {arg: 'status', type: 'string'}
  })

  Graph.remoteMethod('deleteTriples', {
    http: {path: '/deleteTriples', verb: 'post'},
    accepts: {arg: 'graphTriples', type: 'object'},
    returns: {arg: 'status', type: 'string'}
  })

};
