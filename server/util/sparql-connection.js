'use strict';

function SparqlConnection() {
    var Sparql = require('virtuoso-sparql-client');
    var Client = new Sparql.Client("http://10.194.156.23:8890/sparql");
    Client.setOptions("application/json");
    return Client;
}

module.exports = new SparqlConnection();

