const n3 = require("n3");



function getTripleFromQuad(quad) {
  return {
    "g": {
      "type": quad["graph"]["termType"] ? quad["graph"]["termType"] : "",
      "value": quad["graph"]["value"] ? quad["graph"]["value"] : ""
    },
    "subject": {
      "type": quad["subject"]["termType"] ? quad["subject"]["termType"] : "",
      "value": quad["subject"]["value"] ? quad["subject"]["value"] : ""
    },
    "predicate": {
      "type": quad["predicate"]["termType"] ? quad["predicate"]["termType"] : "",
      "value": quad["predicate"]["value"] ? quad["predicate"]["value"] : ""
    },
    "object": {
      "type": quad["object"]["termType"] ? quad["object"]["termType"] : "",
      "value": quad["object"]["value"] ? quad["object"]["value"] : ""
    }
  }
}

function getTriplesFromFileContent(fileContent) {
  let parser = new n3.Parser();
  let quads = [];
  return new Promise(
    function (resolve, reject) {
      parser.parse(fileContent, (error, quad, prefixes) => {
        if (error) {
          reject(error);
        } else if (quad !== null) {
          quads.push(getTripleFromQuad(quad));
        } else {
          resolve(quads);
        }
      })
    }
  )
}


module.exports = {
  "getTriplesFromFileContent": getTriplesFromFileContent
}
