'use strict';

var Parser = require('../util/parser-service');

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  router.post('/api/parseFile', function(req, res) {
    if (!req.body.fileContent) {
      return res.status(400).send({
        'error': 'Please provide data parameter "fileContent"',
      });
    }
    Parser
      .getTriplesFromFileContent(req.body.fileContent)
      .then(function(result) {
        res.send({'triples': result});
      }, function(error) {
        res.status(error).send(error);
      });
  });
  server.use(router);
};
